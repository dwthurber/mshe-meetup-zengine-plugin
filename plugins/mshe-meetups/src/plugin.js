/**
 * Plugin MSHE Meet Ups Controller
 */
plugin.controller('msheMeetUpsCntl', ['$scope', 'znData', '$routeParams', '$location', '$filter', 'znMessage', '$http', 'znModal', function ($scope, znData, $routeParams, $location, $filter, znMessage, $http, znModal) {

    $scope.quarter = 'Spring 2016'

    // Default page
    $scope.currentPage = 'dashboard';

    // Read page from $routeParams
    if ($routeParams.page) {
        $scope.currentPage = $routeParams.page;
    }

	// Current Workspace ID from Route
    $scope.workspaceId = null;

    // Initialize for Workspace ID
    if ($routeParams.workspace_id) {
        // Set Selected Workspace ID
        $scope.workspaceId = $routeParams.workspace_id;
    }

	znData('FormRecords').query({ workspaceId:$scope.workspaceId, formId:42256, limit:500 }, function(data) {
        $scope.students = data;
        // console.log(data[0]);
    });

    znData('FormRecords').query({ workspaceId:$scope.workspaceId, formId:42255, limit:500 }, function(data) {
        $scope.alumni = data;
        // console.log(data[0]);
    });

    znData('FormRecords').query({ workspaceId:$scope.workspaceId, formId:42257, limit:500 }, function(data) {
        $scope.matches = data;
        // console.log(data[0]);
    });

    znData('FormRecords').query({ workspaceId:$scope.workspaceId, formId:48096, limit:500 }, function(data) {
        $scope.quarters = data;
        var lastQuarter = data[data.length -1];
        $scope.quarter = lastQuarter;
    });

    $scope.greaterThan = function(prop, val){
        return function(item){
          return item[prop] >= val;
        };
    };

    $scope.sorter = 'created';

    $scope.order = function(val) {
        $scope.sorter = val;
    };

    $scope.search = '';

    $scope.profileSorter = '-field546404';

    $scope.profileOrder = function(val) {
        $scope.profileSorter = val;
    };

    $scope.StudentNumber = {};
    $scope.AlumnusNumber = {};

    $scope.setStudentNumber = function(index) {
        $scope.StudentNumber = index;
    };
    $scope.setAlumnusNumber = function(index) {
        $scope.AlumnusNumber = index;
    };

    $scope.filteredMatches = [];

    $scope.communicationType = 'students';

    $scope.communicationChange = function(val) {
        $scope.communicationType = val;
        $scope.studentId = null;
        $scope.StudentNumber = {};
        $scope.studentName = null;
    };

    $scope.pickQuarter = function(val){
        var matchQuarter = $filter('uppercase')($scope.quarter.name);
        // console.log(matchQuarter);

        $scope.filteredMatches = ($filter('filter')($scope.matches, {'field410925':val}));
        // console.log($scope.filteredMatches);

        angular.forEach($filter('filter')($scope.students, {'field356694':val}), function(ff){

        });
    };

    $scope.reset = function() {
        $scope.quarter = '';
        $scope.search = '';
        $scope.communicationType = 'students';
    };

    $scope.interests = [];

    $scope.sendData = function(val) {
        $scope.loading = true;
        $scope.ffData = {
            matches: [],
        };

        angular.forEach($filter('filter')($scope.matches, {'field410925':val}), function(ff){
            $scope.ffData.matches.push({
                student: ff.field356698.name,
                alumni: ff.field356696.name,
                quarter: ff.field410925
            });
        });

        $http({
                method: 'POST',
                url: 'https://www.webmerge.me/merge/151221/x9b2iy',
                headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
                data: $.param($scope.ffData),
            }).
            then(function(response) {
              $scope.status = response.status;
              $scope.data = response.data;
              console.log("success");
              console.log($scope.ffData);
              znMessage('Download successful. Please check your email', 'saved');
            }, function(response) {
              $scope.data = response.data || "Request failed";
              $scope.status = response.status;
              console.log("error code " + response.status);
              znMessage('Uh oh, something went wrong sending that download', 'error');
          }).finally(function() {
            // called no matter success or failure
            $scope.loading = false;
          });
    };

    $scope.pickStudent = function(newVal){
        studentId = newVal.id;
        $scope.studentId = newVal.id;
        $scope.studentName = newVal.name;
        $scope.studentQuarter = newVal.field356694;
        $scope.studentEmail = newVal.field357916;


        var interests = [];

        $scope.matched = [];

        $scope.matched = {
            student: [],
            alumnus: [],
            id: []
        };

        angular.forEach($filter('filter')($scope.students, {id:studentId}), function(ff){
            interests = {
                area: ff.field356707,
                institution: ff.field358637,
                jobLevel: ff.field358633,
            };
            studentLocation = ff.field361435,
            studentName = ff.name;
        });


        angular.forEach($filter('filter')($scope.matches, {field356698:{id:studentId}}), function(ff) {
            $scope.matched.student.push(ff.field356698.name);
            $scope.matched.alumnus.push(ff.field356696.name);
            $scope.matched.id.push(ff.field356696.id);
        });
        console.log($scope.matched);
        // console.log($scope.studentId);

        $scope.matchStrength = [];
        $scope.match = [];

        angular.forEach($filter('filter')($scope.alumni, {field546404:'true'}), function(ff) {
            if (ff.field356706 !== null) {
                jobArea = ff.field356706;
            } else {
                jobArea = "blank";
            }
            alumnusLocation = ff.field364546;
            institutionalType = ff.field363985;
            experience = ff.field358083;

            var matchArea = false;
            var matchType = false;
            var matchExperience = false;
            var matchStrength = 0;
            var jobMatchStrength = 0;

            angular.forEach(interests.area, function(key){
                if (jobArea.indexOf(key) >= 0) {
                    matchArea = true;
                    $scope.match.push(ff.name);

                    var x = 0;
                    x++;

                    jobMatchStrength += x;
                }
            });

            if (interests.institution == "No Preference") {
                matchStrength += 17;
            } else {
                angular.forEach(interests.institution, function(key){
                    if (institutionalType == key) {
                        matchType = true;
                        $scope.match.push(ff.name);
                        matchStrength += 18;
                    }
                });
            }

            angular.forEach(interests.jobLevel, function(key){
                if (experience == key) {
                    matchExperience = true;
                    $scope.match.push(ff.name);
                    matchStrength += 18;
                }
            });

            if (studentLocation == 'Other' && alumnusLocation == 'Chicago') {
                $scope.match.push(ff.name);
                matchStrength += 10;
            } else if (studentLocation == 'Other' && alumnusLocation == 'Other') {
                $scope.match.push(ff.name);
                matchStrength += 9;
            } else {
                if (alumnusLocation == 'Chicago') {
                    $scope.match.push(ff.name);
                    matchStrength += 10;
                } else if (alumnusLocation == 'Other') {
                    matchStrength -= 3;
                }
            }

            if (jobMatchStrength >= 4) {
                y = 49;
            } else if (jobMatchStrength == 3) {
                y = 47;
            } else if (jobMatchStrength == 2) {
                y = 45;
            } else if (jobMatchStrength == 1) {
                y = 43;
            } else {
                y = 0;
            }

            if (matchArea === true && matchExperience === true && matchType === true) {
                matchStrength += 5;
            } else if (matchArea === true && (matchExperience === true || matchType === true)) {
                matchStrength += 2;
            } else if (matchType === true && (matchExperience === true || matchArea === true)) {
                matchStrength += 2;
            } else if (matchArea !== true && matchExperience !== true && matchType !== true) {
                matchStrength -= 8;
            }

            var strength = {
                name: ff.name,
                id: ff.id,
                value: matchStrength + y,
            };

            $scope.matchStrength.push(strength);
        });

        // console.log($scope.matchStrength);

        // Send email
        $scope.emailTo = $scope.studentEmail;
        $scope.emailCC = '';
        $scope.emailBCC = 'carla.cortes@northwestern.edu';
        $scope.emailSubject = 'MSHE Meetup Match for ' + $scope.studentQuarter;
        $scope.emailBody = "Dear " + $scope.studentName + ",\n\nThank you for participating in the MSHE MeetUp program. Please see below for your MeetUp partner(s).\n\nPlease reach out to your partner by sending an email to introduce yourself and set up a time for your one-hour conversation. Your partner expects to hear from you soon. Please reach out to your alum by sending an email to introduce yourself and set up a time for your conversation. Be sure to include a short summary of your background and objectives for the conversation.\n\nFinally, please see the MeetUp Rules of Engagement (available at https://northwestern.app.box.com/s/2fb1i8kkttq8q6h6toblw8veqaworpwg) for tips on how to prepare for your MeetUp and follow-up afterward (see slide 8 and onward). Slide 11 has sample questions to consider. It’s important to remember that higher ed is a very small world, and these interactions, while casual, can have great impact. Please prepare appropriately and represent yourself and the program professionally and positively during your conversations.\n\nWe will be very interested to hear how your MeetUp experience goes. I will distribute a survey to solicit your perspectives and ideas for improvement at the end of the quarter.\n\nPlease don't hesitate to contact me with any questions. Have a wonderful MeetUp!\n\nSincerely,\nCarla Cortes";
        $scope.emailMatch = [];

        angular.forEach($scope.matched.id, function(ff) {
            console.log(ff);

            angular.forEach($filter('filter')($scope.alumni, ff), function(ff) {
                var match = {
                    name: ff.name,
                    title: ff.field356686,
                    institution: ff.field356687,
                    email: ff.field356691
                };

                $scope.emailMatch.push(match);
            });
        });

        console.log($scope.emailMatch);

        $scope.sendEmail = function() {
            $scope.email = [];

            $scope.email = {
                name: $scope.studentName,
                to: $scope.emailTo,
                cc: $scope.emailCC,
                bcc: $scope.emailBCC,
                subject: $scope.emailSubject,
                body: $scope.emailBody,
                match: $scope.emailMatch,
            };

            $http({
                    method: 'POST',
                    url: 'https://hooks.zapier.com/hooks/catch/734057/97hfgp/',
                    headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
                    data: $.param($scope.email),
                }).
                then(function(response) {
                  $scope.status = response.status;
                  $scope.data = response.data;
                  console.log("success");
                  console.log($scope.email);
                  znMessage('Email sent', 'saved');
                }, function(response) {
                  $scope.data = response.data || "Request failed";
                  $scope.status = response.status;
                  console.log("error code " + response.status);
                  znMessage('Uh oh, something went wrong sending that email', 'error');
              }).finally(function() {
                // called no matter success or failure
                $scope.loading = false;
              });
        };

    };



    $scope.onMatchAlumnus = function(val) {
        alumnusId = val.field356696.id;
        $scope.alumnusId = alumnusId;

        znModal({
            title: 'test',
            templateUrl: "mshe-meet-ups-alumnus",
            classes: 'my-dialogue',
            scope: $scope,
            btns: {
                'Add Match': {
                    success: true,
                    action: function(newVal) {
                        var saveObj = {};
                            saveObj.field356696 = alumnusId;
                            saveObj.field356698 = studentId;
                            saveObj.field356680 = val.field356696.name + ' + ' + $scope.studentName;
                            saveObj.field410925 = $scope.studentQuarter;

                        znData('FormRecords').save({formId:42257}, saveObj, function(response) {
                            console.log(response);
                            znMessage('Match added', 'saved');

                            znData('FormRecords').get({workspaceId:$scope.workspaceId, formId:42256, limit:500}, function(data){
                                $scope.students = data;
                            });

                            znData('FormRecords').get({workspaceId:$scope.workspaceId, formId:42255, limit:500}, function(data){
                                $scope.alumni = data;
                            });

                            znData('FormRecords').get({workspaceId:$scope.workspaceId, formId:42257, limit:500}, function(data){
                                $scope.matches = data;
                            });

                        });
                    }
                },
                'Remove Match': {
                    danger: true,
                    action: function(newVal) {
                        angular.forEach($filter('filter')($scope.matches, {field356696:{id:alumnusId}}, {field356698:{id:studentId}}), function(ff) {
                            deleteObj = ff.id;
                        });

                        znData('FormRecords').delete({formId:42257, id:deleteObj}, function() {
                            znMessage('Match removed', 'saved');

                            znData('FormRecords').get({workspaceId:$scope.workspaceId, formId:42257, limit:500}, function(data){
                                $scope.matches = data;
                            });

                            znData('FormRecords').get({workspaceId:$scope.workspaceId, formId:42256, limit:500}, function(data){
                                $scope.students = data;
                            });

                            znData('FormRecords').get({workspaceId:$scope.workspaceId, formId:42255, limit:500}, function(data){
                                $scope.alumni = data;
                            });
                        });
                    }
                }
            }
        });
    };


    $scope.onAlumnus = function(val) {
        alumnusId = val.id;
        $scope.alumnusId = alumnusId;
        $scope.alumnusMatchScore = val.value;

        znModal({
            title: 'test',
            templateUrl: "mshe-meet-ups-alumnus",
            classes: 'my-dialog',
            scope: $scope,
            btns: {
                'Add Match': {
                    success: true,
                    action: function(newVal) {
                        var saveObj = {};
                            saveObj.field356696 = alumnusId;
                            saveObj.field356698 = studentId;
                            saveObj.field356680 = val.name + ' + ' + $scope.studentName;
                            saveObj.field410925 = $scope.studentQuarter;

                        znData('FormRecords').save({formId:42257}, saveObj, function(response) {
                            console.log(response);
                            znMessage('Match added', 'saved');

                            znData('FormRecords').get({workspaceId:$scope.workspaceId, formId:42256, limit:500}, function(data){
                                $scope.students = data;
                            });

                            znData('FormRecords').get({workspaceId:$scope.workspaceId, formId:42255, limit:500}, function(data){
                                $scope.alumni = data;
                            });

                            znData('FormRecords').get({workspaceId:$scope.workspaceId, formId:42257, limit:500}, function(data){
                                $scope.matches = data;
                            });

                        });
                    }
                },
                'Remove Match': {
                    danger: true,
                    action: function(newVal) {
                        angular.forEach($filter('filter')($scope.matches, {field356696:{id:alumnusId}}, {field356698:{id:studentId}}), function(ff) {
                            deleteObj = ff.id;
                        });

                        znData('FormRecords').delete({formId:42257, id:deleteObj}, function() {
                            znMessage('Match removed', 'saved');

                            znData('FormRecords').get({workspaceId:$scope.workspaceId, formId:42257, limit:500}, function(data){
                                $scope.matches = data;
                            });

                            znData('FormRecords').get({workspaceId:$scope.workspaceId, formId:42256, limit:500}, function(data){
                                $scope.students = data;
                            });

                            znData('FormRecords').get({workspaceId:$scope.workspaceId, formId:42255, limit:500}, function(data){
                                $scope.alumni = data;
                            });
                        });
                    }
                }
            }
        });
    };



}])

/**
 * Plugin MSHE Meet Ups Settings Controller
 */
.controller('msheMeetUpsSettingsCntl', ['$scope', 'znData', '$routeParams', function ($scope, znData, $routeParams) {

}])
