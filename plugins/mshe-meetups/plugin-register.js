plugin.register('msheMeetUps', {
	route: '/mshemeetups',
	title: 'MeetUp Matching',
	icon: 'icon-users',
	interfaces: [
		{
			controller: 'msheMeetUpsCntl',
			directive: 'msheMeetUpsDirective',
			template: 'mshe-meet-ups-main',
			type: 'fullPage',
			order: 300,
			topNav: true,
			routes: [
				'/:page'
			]
		}
	]
});
